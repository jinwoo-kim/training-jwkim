
var util = require('../dist/output.js');


describe('Hello World', function() {
    it('speak hello', function() {
        expect(util.hello()).toEqual("hello");
    });
    it('speak world', function() {
        expect(util.world()).toEqual("world");
    });
});



