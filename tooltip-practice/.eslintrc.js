module.exports = {
    "extends": "tui",
    "parserOptions": {
        "ecmaVersion": 3,
        "sourceType": "module"
    },
    "env": {
        "commonjs": true,
        "jasmine": true,
        "amd": true,
        "node": true,
        "jquery": true
    },
    "globals": {
        "loadFixtures": true
    }
}
